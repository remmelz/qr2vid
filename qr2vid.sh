#!/bin/bash

# Defaut settings
_cache="./cache"
_split_lines=5
_qr_size=3       # specify the size of dot (pixel)
_qr_level=L      # Error correction level (LMQH)
_qr_dpi=72

##############################
# Precheck
##############################

_input=$1
_cwd=`pwd`

if [[ ! -f ${_input} || -z ${_input} ]]; then
  echo "Error: no input file given."
  exit 1
fi

_error=0
which bc       > /dev/null || _error=1
which ffmpeg   > /dev/null || _error=1
which mencoder > /dev/null || _error=1
which mplayer  > /dev/null || _error=1
which qrencode > /dev/null || _error=1
which zbarimg  > /dev/null || _error=1

if [[ ${_error} -gt 0 ]]; then
  echo "Error: one or more required commands missing."
  exit 1
fi

mkdir -p ${_cache}

##############################
# Encoding
##############################

if [[ -z `echo ${_input} | grep "avi$"` ]]; then

  echo " -> Encode file to base64..."
  base64 ${_input} | sed 's/^/b64:/g' > ${_cache}/out.b64

  cd ${_cache}
  
  echo " -> Splitting base64..."
  split -l${_split_lines} -a10 out.b64

  c=1
  echo " -> Storing meta data per chunk..."
  for b in `ls x*`; do
    _md5sum=`md5sum ${b} | cut -c-6`
    echo "md5:${_md5sum}" >> ${b}
    echo "seq:${c}"       >> ${b}
    let c=${c}+1
  done
 
  echo " -> Storing original file information..."
  _md5sum=`md5sum ${_cwd}/${_input} | awk -F' ' '{print $1}'`
  _chunks=`ls -1 x* | wc -l`
  _size=`du -h ${_cwd}/${_input} | awk -F' ' '{print $1}'`
  echo "file: ${_input}"    >> ./xzzzzzzzzzz 
  echo "size: ${_size}"     >> ./xzzzzzzzzzz 
  echo "md5sum: ${_md5sum}" >> ./xzzzzzzzzzz
  echo "chunks: ${_chunks}" >> ./xzzzzzzzzzz

  echo " -> Encoding data chunks to images..."
  for b in `ls x*`; do
    qrencode \
      --size=${_qr_size} \
      --level=${_qr_level} \
      --dpi=${_qr_dpi} \
      -r ${b} -o ${b}.png
    rm ${b}
  done
  
  echo " -> Encoding images to video..."
  mencoder mf://*.png \
    -mf w=800:h=600:fps=25:type=png \
    -ovc copy -oac copy -o out.avi \
    > /dev/null 2>&1

  _filename=`echo ${_input} | awk -F'.' '{print $1}'`
  mv out.avi ${_cwd}/${_filename}.avi
  echo " -> Completed ${_filename}.avi"

else

##############################
# Decoding
##############################

  echo " -> Converting video to images..."
  ffmpeg -i ${_input} "./${_cache}/%010d.png" \
    > /dev/null 2>&1

  cd ${_cache}

  echo " -> Retrieving info..."
  _info=`ls -1 *.png | sort -n | tail -1`
  zbarimg --raw -q ${_info} \
      | grep -v '^$' > info.txt

  _filename=`grep 'file:'   info.txt | cut -d':' -f2 | sed 's/ //g'`
  _chunks=`  grep 'chunks:' info.txt | cut -d':' -f2 | sed 's/ //g'`
  cat info.txt; rm ${_info}

  _perc10=`echo "(${_chunks}/100)*10" | bc`
  _perc20=`echo "(${_chunks}/100)*20" | bc`
  _perc30=`echo "(${_chunks}/100)*30" | bc`
  _perc40=`echo "(${_chunks}/100)*40" | bc`
  _perc50=`echo "(${_chunks}/100)*50" | bc`
  _perc60=`echo "(${_chunks}/100)*60" | bc`
  _perc70=`echo "(${_chunks}/100)*70" | bc`
  _perc80=`echo "(${_chunks}/100)*80" | bc`
  _perc90=`echo "(${_chunks}/100)*90" | bc`

  c=1
  echo " -> Decoding QR codes..."
  echo -n " -> Progress ..1%"
  for i in `ls *.png | sort -n`; do

    [[ ${c} -eq ${_perc10} ]] && echo -n '..10%'
    [[ ${c} -eq ${_perc20} ]] && echo -n '..20%'
    [[ ${c} -eq ${_perc30} ]] && echo -n '..30%'
    [[ ${c} -eq ${_perc40} ]] && echo -n '..40%'
    [[ ${c} -eq ${_perc50} ]] && echo -n '..50%'
    [[ ${c} -eq ${_perc60} ]] && echo -n '..60%'
    [[ ${c} -eq ${_perc70} ]] && echo -n '..70%'
    [[ ${c} -eq ${_perc80} ]] && echo -n '..80%'
    [[ ${c} -eq ${_perc90} ]] && echo -n '..90%'

    zbarimg --raw -q ${i} > ${i}.raw
    grep '^md5:' ${i}.raw > ${i}.md5
    grep '^b64:' ${i}.raw > ${i}.b64

    _md5sum=`md5sum ${i}.b64 | cut -c-6`
    _md5chk=`cat ${i}.md5 | cut -d':' -f2`

    if [[ ${_md5sum} != ${_md5chk} ]]; then
      echo "Error: md5sum mismatch at ${i}!"
      exit 1
    else
      cat ${i}.b64 \
        | sed 's/^b64://g '>> out.b64
    fi
    let c=${c}+1
  done

  _md5sum=`  grep 'md5sum:' info.txt | cut -d':' -f2 | sed 's/ //g'`

  echo "..100%"
  echo " -> Converting base64 to file"
  base64 --decode out.b64 > out

  echo " -> Verifying output file"
  mv out ${_filename}
  echo "${_md5sum} ${_filename}" > md5sum.txt
  md5sum --check md5sum.txt

  echo " -> Completed ${_filename}"
  mv -i ${_filename} ${_cwd}

fi

# Cleanup 
cd ${_cwd}
rm ${_cache}/*
rmdir ${_cache}

exit 0

