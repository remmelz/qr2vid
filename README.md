
# qr2vid

Convert files to videos by splitting the original file into chunks and encode
it using QR-encode and Mencoder

## Requirements

The following tools are required:

* mencoder
* ffmpeg or mplayer
* qrencode
* zbar-tools


## Getting started

To encode a file to a QR video:

    ./qr2vid.sh <input_file>
  
To convert it back:

    ./qr2vid.sh <input_video>


